package com.vsu.framespark.mvvm.ViewModel

import androidx.lifecycle.ViewModel
import com.vsu.framespark.mvvm.Model.Dog
import com.vsu.framespark.mvvm.Model.DogRepository

class DogViewModel(val dogRepository: DogRepository) : ViewModel() {
    val data: DogRepository

    init {
        data = dogRepository
        data.addDog(Dog(1,"Pes", "Shpitz"))
    }
}