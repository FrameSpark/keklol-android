package com.vsu.framespark.mvvm.Model

data class Dog(val id: Int, val name: String, val breed: String)