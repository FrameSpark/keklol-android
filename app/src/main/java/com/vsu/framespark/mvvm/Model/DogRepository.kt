package com.vsu.framespark.mvvm.Model

import java.util.*

class DogRepository {
    
    val dogs : MutableList<Dog> = ArrayList()

    fun getDogByName(name: String) : Dog{
        for(dog: Dog in dogs){
            if(dog.name == name)
                return dog
        }
        throw Exception("Dog not found")
    }

    fun getFirstDog() : Dog{
        return dogs.get(0)
    }

    fun addDog(dog: Dog) {
        dogs.add(dog)
    }

    fun deleteDogByName(name: String){
        for(dog: Dog in dogs){
            if(dog.name == name)
                dogs.remove(dog)
        }
        throw Exception("Dog not found")
    }
}