package com.vsu.framespark.mvvm.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.vsu.framespark.mvvm.Model.Dog
import com.vsu.framespark.mvvm.Model.DogRepository
import com.vsu.framespark.mvvm.R
import com.vsu.framespark.mvvm.ViewModel.DogViewModel

class MainActivity : AppCompatActivity() {

    private var dogViewModel: DogViewModel = DogViewModel(DogRepository())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun getFirstDog(view: View){
        var dog: Dog = dogViewModel.dogRepository.getFirstDog()
        val textViewName = findViewById<TextView>(R.id.name)
        val textViewBreed = findViewById<TextView>(R.id.breed)
        textViewName.text = dog.name
        textViewBreed.text = dog.breed

    }
}